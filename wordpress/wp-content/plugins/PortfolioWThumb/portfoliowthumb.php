<?php


/*
Plugin Name: Portfolioposts with thumbnails ST
Plugin URI: http://blog.studiotriangular.se/wp-plug-pimgwthumbs
Description: This plugin creates a custom post type with custom taxonomies. The post type acts as a portfolio image display where there is a main image and a thumbnail that can be displayed with custom categories.
Version: 1.0
Author: Mikael Biström
	Author URI: http://studiotriangular.se
License: GPL2
*/

/*  Copyright 2011 Mikael Biström  (email : info@studiotriangular.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/* CUSTOM POST - STYRELSEMEDLEMMAR 
---------------------------------------------------------------------------------------*/

add_action('admin_init', 'pwtist_admin_init');
add_action('admin_menu' , 'pwtist_admin_menu');



//  Add Columns to Portfolio Edit Screen 
add_filter("manage_edit-pwtist_post_columns", "portfolio_edit_columns");
add_action("manage_posts_custom_column",  "portfolio_columns_display", 10, 2);
add_action('do_meta_boxes', 'customposttype_image_box');
 
function portfolio_edit_columns($portfolio_columns){
    $portfolio_columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "title" => _x('Title', 'column name'),
        "thumbnail" => __('Thumbnail'),
        "portfolio-cats" => __('Bildserie'),
        "portfolio-tags" => __('Taggar'),
        "date" => __('Date')
    );
    return $portfolio_columns;
}




function portfolio_columns_display($portfolio_columns, $post_id){
    switch ($portfolio_columns)
    {
        case "thumbnail":
            $width = (int) 60;
            $height = (int) 60;
                $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
                // image from gallery
                $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
                if ($thumbnail_id)
                    $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
                elseif ($attachments) {
                    foreach ( $attachments as $attachment_id => $attachment ) {
                        $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
                    }
                }
                    if ( isset($thumb) && $thumb ) {
                        echo $thumb;
                    } else {
                        echo __('None');
                    }
            break;
        case "portfolio-tags":
            if ($tag_list = get_the_term_list( $post->ID, 'pwtist_tag', '', ', ', '' ) ) {
                echo $tag_list;
            } else {
                    echo __('None');
                }
            break;
        case "portfolio-cats":
        	if ($cat_list = get_the_term_list( $post->ID, 'pwtist_cat', '', ', ', '' ) ) {
        		echo $cat_list;
        	} else {
        		echo __('None');
        	}
    }
}

function pwtist_admin_init() {
          wp_register_script('draganddropsort', plugins_url('/js/jquery.draganddropsort.js', __FILE__), array('jquery'));
          wp_enqueue_script('draganddropsort');
}

function pwtist_admin_menu(){
	global $pwtist_manage_page;
	$pwtist_manage_page = add_submenu_page('edit.php?post_type=pwtist_post', 
							 'Inställningar för visning av bildserier', 
							 'Inställningar', 
							 'edit_posts', 
							 basename(__FILE__), 
							 'pwtist_manage_menu');
	add_action( "admin_head-{$pwtist_manage_page}", 'pwtist_admin_styles' );					 
	
}

function pwtist_admin_styles(){
	?>
		<link rel="stylesheet" type="text/css" href="<?php echo plugins_url('/css/pwtist_style.css', __FILE__); ?>">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
		<script>
		jQuery(document).ready(function($) {
			$('#drag-content').draganddropsort();
			$('#drag-content .ddsitem:last').addClass('last-item');
		});
		</script>
	<?php
}
function pwtist_testArrays($c, $s) {
	$result=array();
	for ($i=0; $i < count($s); $i++) {
		$noMatch = true;	
		for ($j=0; $j < count($c); $j++) { 
			if(key($s[$i])===key($c[$j])) {
				$noMatch = false;
				$result[] = $c[$j];
				unset($c[$j]);
			}
		}
		if($noMatch) {
			unset($s[$i]);
		}
	}
	$result = array_merge($result, $c);
	return $result;
}


function pwtist_manage_menu(){
	global $title;
	get_option('category_order') == "" ? "" : $saved_vals = unserialize(get_option('category_order'));
	$hidden_field_name = 'att_submit_hidden';
	$args = array(	'taxonomy' => 'pwtist_cat',
					'order_by' => 'name',
					'hide_empty' => 1);
	$comp_cats = array();
	$categories = get_categories($args);
	foreach ($categories as $category) {
		$comp_cats[] = array($category->slug => $category->name);
	}
	$combined = pwtist_testArrays($comp_cats, $saved_vals);
	?>
	<h1 style="margin-left:60px;">Sortera kategorier.</h1>
	<?php
		//SAVING ORDER
		if(isset($_POST[$hidden_field_name]) && $_POST[$hidden_field_name] == 'Y') {
				$combined = array();
			foreach ($_POST as $key => $value) {
				if ($value !== 'Y') {
					$combined[] = array($key =>$value);
				}
			}
			update_option('category_order', serialize($combined)); ?>
		<div style="margin-left:60px; margin-top:60px;" id="message" class="updated fade">
			<p><strong>
				<?php _e('Options saved.', 'att_trans_domain' ); ?>
			</strong></p>
		</div>
		<?php
		}
	?>
	<form name="pwtist_cat_options" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<div id="drag-content">
		<?php
        foreach ($combined as $key) : ?>
		<?php foreach ($key as $k => $v) : ?>
        		<input class="ddsitem" name="<?php echo $k; ?>" value="<?php echo $v; ?>"/>
        	<?php
        endforeach;
        endforeach;
		?>
		</div>
		<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
		<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
	</form>
	<?php
	?>
		
		<div id="instructions">
			<h3>Instruktioner.</h3>
			<p>Här kan du sortera dina kategorier. Detta sker genom "drag and drop". När du är nöjd med resultatet så trycker pu på Save Changes längst ner på listan.</p>
			<p>Ordningen kommer att visas på alla sidor. Dock har kanske inte alla kategorier bilder, men det är lugnt. De kategorier som har bilder kommer att visas.</p>
		</div>
	<?php
}






function customposttype_image_box() {
	add_meta_box('postimagediv', __('Custom Image'), 'post_thumbnail_meta_box', 'pwtist_post', 'normal', 'high');
}






?>