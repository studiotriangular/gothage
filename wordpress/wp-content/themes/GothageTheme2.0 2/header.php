<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title><?php echo bloginfo('name'); ?> - <?php echo bloginfo('description'); ?></title>
  <meta name="Subject" content="Photographer, Fotograf, Photography, Fotografi">
  <meta name="Keywords" content="fotograf, photographer, reklamfotografi, advertising photography, livsstilsfotografi, lifestyle photography, modefotografi, fashion photography, stilleben, still life, digital fotografi, digital photography, kreativ, creative, retusch, retouching, göteborg, gothenburg, sverige, sweden, livsstil, lifestyle, reklam, advertising, digital, analog, analouge, studio, porträtt, portrait, reklamfotograf, advertising photograhper, fotografi, photography, digitalfoto, digital photo, bilder, images, imagery, photographs, musikfotografi, music photography, musikfoto, music photo, musik, music">
  <meta name="Description" content="Fotograf Mikael Göthage, Göteborg. Photographer Mikael Gothage, Gothenburg.">  
<?php wp_head(); ?>
</head>
<body>
	<header class="hidden">
		<h1><a href="<?php echo home_url(); ?>">Mikael Göthage</a></h1>
		<img src="<?php bloginfo('template_directory'); ?>/images/logoblack.png" alt="Mikael Göthage Photographs logo"/>
	</header>
	<div id="content">