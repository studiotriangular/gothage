$(document).ready(function(){
	var swipeBreakPoint = 1024, ivContentMargin, windowSize = $(window).width(),swipeThis=false;
	
	

	if( swipeBreakPoint > windowSize ) {
		swipeThis = true;
	}
	window.onresize = function(e){
		windowSize = $(window).width();
		if( swipeBreakPoint > windowSize ) {
			swipeThis = true;

		} else {
			swipeThis = false;
		}
	};


	// MENU FUNCTIONS
	$('nav li a').hover(function(){
		toggleArticle($(this).attr('href'));
	});
	function toggleArticle(string){
		$(string).toggleClass('show');
	}

	// HANDLE CATEGORY SELECTION
	var fade = true;
	$('#gothage-index div ul li a').on('click', function(e){
		var curClass = $(this).attr('href');
		$('#thumb-chart').addClass('hidden');
		setTimeout(function(){
			$('#thumb-chart').removeClass('hidden');
			$('a.thumbnail-image').each(function(){
					if(!$(this).hasClass(curClass)) { $(this).addClass('inactive'); }
					if($(this).hasClass(curClass) && $(this).hasClass('inactive')) { $(this).removeClass('inactive'); }
			});
			setThumbArray();
		},800);
		e.preventDefault();
		
		
	});



	// SET SWIPE STUFF

	// IMAGE VIEWER START
	var thumbArray, lastArray, videoArray, imagesToLoad;
	ivWrapper    = $('#iv-wrapper');
	ivContent    = $('#iv-content');
	
	lastArray    = [];
	videoArray   = [];
	imagesToLoad = [];




	setThumbArray();
	
	function setThumbArray(){
		var videoObject = [];
		thumbArray   = [];
		$('a.thumbnail-image').each(function(){
			if ( !$(this).hasClass('inactive') ) {
				var url = $(this).attr('href');
				if( $(this).attr('data-ogg') && $(this).attr('data-mp4') && $(this).attr('data-webm') ) {
					videoObject['ogg'] = $(this).attr('data-ogg');
					videoObject['mp4'] = $(this).attr('data-mp4');
					videoObject['webm'] = $(this).attr('data-webm');
					videoArray[url] = videoObject;
				}
				thumbArray.push(url);
				lastArray[url] = ( $(this).hasClass('add-space-right') ? 1 : 0 );
			}
		});
	}
	

	ivContent.on('click', 'article', function(e){
		e.preventDefault();
		if(!swipeThis) {
			if($(this).hasClass('current-article')) {
				startUpViewer(false);
			} else {
				$('.current-article').removeClass('current-article');
				$(this).addClass('current-article');
				remapClasses();
				calculateMargin(true);
			}
		} else {

		}
		
	});
	ivContent.on('ended', 'video', function(e){
		$(this).fadeOut('fast');
		$(this).siblings('.playbutton').fadeIn('fast');
	});
	ivContent.on('click', 'div', function(e){
		var vObj;
		if($(this).hasClass('playbutton') && !swipeThis) {
			e.stopPropagation();
			vObj = $(this).siblings('video');
			vObj.css('display', 'block');
			vObj.fadeIn('fast');
			$(this).fadeOut('fast');
			vObj[0].play();
			
			
		}
		
	});
	
	ivContent.on('click', 'video', function(e){
		if(!swipeThis) {
			e.stopPropagation();
			$(this).fadeOut('fast');
			
			$(this).get(0).pause();

			$(this).siblings('.playbutton').fadeIn('fast');
		}
	});
	


	/*
		Touch events checking
		--------------------- */
	var hammertime = Hammer(ivContent, {
		transform_always_block : true,
		transform_min_scale : 1,
		drag_block_horizontal: true,
		drag_block_vertical: false,
		drag_min_distance: 0
	});
	var posX=0, posY=0,lastPosX=0,lastPosY=0,bufferX=0,bufferY=0,scale=1,last_scale,rotation=1,last_rotation,dragReady=0,changePoint=windowSize/3;
	/*
	hammertime.on('tap', '.playbutton', function(ev){
		ev.stopPropagation();
		
		alert('hej');



		
		// if (vObj[0].requestFullscreen) {
		//   vObj[0].requestFullscreen();
		// } else if (vObj[0].mozRequestFullScreen) {
		//   vObj[0].mozRequestFullScreen();
		// } else if (vObj[0].webkitRequestFullscreen) {
		//   vObj[0].webkitRequestFullscreen();
		// }
	});
*/
	hammertime.on('touch drag dragend swipe tap', function(ev){
		ev.stopPropagation();
		ev.preventDefault();
		if(swipeThis) manageTouch(ev);
	});

	function manageTouch(ev) {
		ev.stopPropagation();

		switch(ev.type) {

			case 'tap':
				ev.stopPropagation();
				ev.preventDefault();
				$theTarget = ev.target;
				if($theTarget.nodeName === 'DIV' || $theTarget.nodeName === 'VIDEO') {
					if( $theTarget.nodeName === 'DIV' ) {
						vObj = $('.current-article').children('video');
						vObj.css('display', 'box');
						vObj.fadeIn('fast');
						$(this).fadeOut('fast');
						vObj[0].play();
					}
					
				} else {
					startUpViewer(false);
				}

				
				
			break;
			case 'drag':
				
				posX = ev.gesture.deltaX + lastPosX;
				
				ivContent.css('margin-left', ivContentMargin+posX);
			break;
			case 'dragend':
				if( posX>changePoint ) {
					swipeNext(true);
				} else if( posX < (changePoint*-1) ) {
					swipeNext(false);
				} else {
					ivContent.animate({'marginLeft': ivContentMargin},'fast');
				}
				
			break;
			case 'swipe':
			break;
		}
	}
	function swipeNext(next){
		var firstArticle, firstArticleWidth;
		firstArticle      = $('#iv-content article').first();
		lastArticle       = $('#iv-content article').last();
		firstArticleWidth = firstArticle.outerWidth();
		$('.current-article').removeClass('current-article');
		if(next){
			ivContent.animate({'marginLeft': 0},'fast', function() {
				
				firstArticle.addClass('current-article');
				insertArticle(getNewUrl(false), true);
				ivContent.css( 'marginLeft' , ivContentMargin );
				lastArticle.remove();
				remapClasses();
			});
			
		}else {
			ivContent.animate({'marginLeft': '-'+firstArticleWidth*2},'fast', function(){
				lastArticle.addClass('current-article');
				insertArticle(getNewUrl(true), false);
				ivContent.css( 'marginLeft' , ivContentMargin );
				firstArticle.remove();
				remapClasses();
			});
		}
		
	}


	function startUpViewer(really) {
		if(really) {
			ivWrapper.removeClass('closed');
			$('#thumb-chart').addClass('hidden');
			$('header img').addClass('hidden');
			if(swipeThis) {
				$('#thumb-chart').hide();
				$('header').hide();
				$('footer').hide();
			}
		} else {
			ivWrapper.addClass('closed');
			setTimeout(function(){ivContent.empty();}, 800);
			$('#thumb-chart').removeClass('hidden');
			$('header img').removeClass('hidden');
			if (swipeThis) {
				setTimeout(function(){
					// $('#thumb-chart').removeClass('hidden');
					$('#thumb-chart').fadeIn('fast');
				},300);
				$('header').show();
				$('footer').show();
			}
		}
	}
	
	$('.cat-holder a').click(function(e){
		var currentUrl = $(this).attr('href');
		var indexesToLoop = [];
		e.preventDefault();
		
		indexesToLoop.push($.inArray(currentUrl, thumbArray));
		indexesToLoop.push( ( thumbArray[indexesToLoop[0]-1] ? $.inArray(thumbArray[indexesToLoop[0]-1], thumbArray) : $.inArray(thumbArray[thumbArray.length -1], thumbArray) ) );
		indexesToLoop.push( ( thumbArray[indexesToLoop[0]+1] ? $.inArray(thumbArray[indexesToLoop[0]+1], thumbArray) : $.inArray(thumbArray[0], thumbArray) ) );
		for ( var i = 0; i < indexesToLoop.length; i++ ) {
			var classes,thisUrl,imtlPrepends;
			thisUrl = thumbArray[indexesToLoop[i]];
			imtlPrepends = false;
			if(i === 0) {
				classes = 'current-article';
			} else if (i == 1) {
				classes = 'prev-article';
				imtlPrepends = true;
			} else if (i == 2) {
				classes = 'next-article';
			}
			if(lastArray[thisUrl]) { classes+= ' last'; }
			var imgObjtoLoad = createToLoadObject(thisUrl, imtlPrepends, classes);
			imagesToLoad.push(imgObjtoLoad);
		}
		startUpViewer(true);
		loadArticles();
		
		
		
		
	});




	// CREATING OBJECT TO BE LOADED IN QUEUE
	function createToLoadObject(thumburl, imtlPrepends, classString) {
		var obj = {
			url: thumburl,
			imtlPrepends: imtlPrepends,
			classes: classString
		};
		return obj;
	}
	function insertArticle(thumburl, imtlPrepends, classString) {
		// $(window).height();
		// var appendString = '<article class="'+classString+' hidden" style="background-image: url('+thumburl+');">', controls, poster;
		var appendString, controls, poster, setheight;
		
		
		
		appendString = '<article class="'+classString+' hidden" >';

		poster = ' poster="'+thumburl+'"';

		if(swipeThis) {
			appendString+='<div class="vertical-helper"></div>';



			
			// appendString = '<article class="'+classString+' hidden" style="background-image: url('+thumburl+');">';
			if( videoArray[thumburl] ) {
				setheight = $(window).width() * 0.56;
				// controls = ' controls style=" top:50%; margin-top:-'+setheight+'px; height:'+setheight+'px"';
				controls = ' controls';
				controls+=poster;
			}
			
			

			
			// controls = ' controls style=" top:50%; margin-top:-'+setheight/2+'px; height:'+setheight+'"';
		} else {
			
			if( videoArray[thumburl] ) {
				controls = ' style=" top:50%; margin-top:-'+setheight/2+'px; height:'+setheight+'px"';
				controls+=poster;
			}


		}
		
		if(videoArray[thumburl]) {
			
			// alert(videoArray[thumburl]['mp4']);
			appendString+= '<video class="video"'+controls+'>';
			
			appendString+= '<source src="'+videoArray[thumburl]['webm']+'" type="video/webm; codecs=\'vp8,vorbis\'" />';
			appendString+= '<source src="'+videoArray[thumburl]['ogg']+'" type="video/ogg; codecs=\'theora, vorbis\'" />';
			appendString+= '<source src="'+videoArray[thumburl]['mp4']+'" type="video/mp4" />';

			appendString+= '</video>';
			if(!swipeThis){ appendString+= '<div class="playbutton"></div>'; }
			else {}
			
		}else{
			// appendString = '<article class="'+classString+' hidden" style="background-image: url('+thumburl+');">';
			
			if(!swipeThis ) {
				
			}
			appendString+= '<img src="'+thumburl+'"/>';
			
		}
		
		appendString += '</article>';
		if(imtlPrepends) {
			ivContent.prepend(appendString);
		} else  {
			ivContent.append(appendString);
		}
		setTimeout(function(){
			$('#iv-content article.hidden').removeClass('hidden');
		}, 100);
		calculateMargin(false);
	}

	function remapClasses() {
		var classToAdd = 'prev-article';
		ivContent.children('article').each(function(i){
			if (!$(this).hasClass('current-article')) {
				$(this).removeClass('prev-article').removeClass('next-article');
				$(this).addClass(classToAdd);
			}
			
			if ($(this).hasClass('current-article')) {
				$('.current-article').removeClass('current-article');
				$(this).addClass('current-article');
				$(this).removeClass('prev-article').removeClass('next-article');
				classToAdd = 'next-article';
			}
		});
	}

	function loadArticles(){
		if(!imagesToLoad[0]){
			ivContentMargin = parseInt(ivContent.css('marginLeft'), 10);
			$('#loading').removeClass('show');
			checkForSpace();
			return;
		}else{
			if( !$('#loading').hasClass('show')  ) {$('#loading').addClass('show');}
			var url = imagesToLoad[0].url;
			var imgObj = new Image();
			imgObj.onload = function() {
				insertArticle(imagesToLoad[0].url, imagesToLoad[0].imtlPrepends, imagesToLoad[0].classes);
				imagesToLoad.shift();
				loadArticles();
			};
			imgObj.src = url;
		}
	}

	function calculateMargin(animate) {
		var includeWidth = true;
		var newMargin = 0;
		var contentWidth = 0;
		var wrapperWidth = ivWrapper.width()/2;
		ivContent.children('article').each(function(i){
			if(includeWidth) {
				if (!$(this).hasClass('current-article')) {
					contentWidth += $(this).outerWidth();
					contentWidth += parseInt($(this).css('marginRight'), 10);
				}
				if ($(this).hasClass('current-article')) {
					contentWidth+=$(this).width()/2;
					includeWidth = false;
				}
			}
		});
		newMargin = wrapperWidth-contentWidth;
		if(animate) { ivContent.stop().animate({'marginLeft' : newMargin}, 500, function(){checkForSpace();}); }
		else {
			ivContent.css('margin-left',newMargin+'px');
		}
	}
	function getNewUrl(getNext) {
		var thisUrl      = '';
		var theURL       = '';
		var firstArticle = $('#iv-content article').first();
		var lastArticle  = $('#iv-content article').last();

		if(getNext) {
			thisUrl = lastArticle.find('img').attr('src');
			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex+1] ? $.inArray(thumbArray[getIndex+1], thumbArray) : $.inArray( thumbArray[0], thumbArray ) );
			theURL = thumbArray[getIndex];
		} else {
			thisUrl = firstArticle.find('img').attr('src');
			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex-1] ? $.inArray(thumbArray[getIndex-1], thumbArray) : $.inArray( thumbArray[thumbArray.length-1], thumbArray ) );
			theURL = thumbArray[getIndex];
		}
		return theURL;
	}

	function checkForSpace() {
		if( !swipeThis ) {
			console.log('checkForSpace is on');
			var firstImage,lastImage,contentLeftMargin,firstArticleWidth,firstArticle,lastArticle,lastArticleOffset,thisUrl,getIndex,theURL,classes,imgObjtoLoad,getLoading;
			firstArticle      = $('#iv-content article').first();
			lastArticle       = $('#iv-content article').last();
			firstArticleWidth = firstArticle.outerWidth();
			lastArticleOffset = lastArticle.offset();
			contentLeftMargin = parseInt(ivContent.css('marginLeft'), 10);
			getLoading = false;
			if( contentLeftMargin < firstArticleWidth*-1 ) {
				$('#iv-content article').first().remove();
				calculateMargin();
			}
			if( contentLeftMargin > 0 ) {
				theURL = getNewUrl(false);
				classes = 'prev-article';
				if(lastArray[theURL]) { classes += ' last';}
				imgObjtoLoad = createToLoadObject(theURL, true, classes);
				imagesToLoad.push(imgObjtoLoad);
				getLoading = true;
			}
			if(lastArticleOffset.left+lastArticle.innerWidth() < ivWrapper.innerWidth()) {
				theURL = getNewUrl(true);
				classes = 'next-article';
				if(lastArray[theURL]) { classes += ' last';}
				imgObjtoLoad = createToLoadObject(theURL, false, classes);
				imagesToLoad.push(imgObjtoLoad);
				getLoading = true;
			}
			if(lastArticleOffset.left > $(document).width()) {
				lastArticle.remove();
			}
			if(getLoading) {
				loadArticles();
			}
		} else {
			//- UNDER SWIPE BREAKPOINT


		}
	}





















});