$(document).ready(function(){
	

	var swipeBreakPoint = 1024;
	var ivContentMargin;
	var windowSize = $(window).width();
	var swipeThis = false;
	var thumbArray = [];
	var lastArray = [];
	var videoArray = [];
	var imagesToLoad = [];
	var startUpReady = true;
	var ivWrapper    = $('#iv-wrapper');
	var ivContent    = $('#iv-content');
	var timer;
	var timeDrag = false;
	var theVideo;

	setThumbArray();
	
	if( swipeBreakPoint > windowSize ) {
		swipeThis = true;
	}
	window.onresize = function(e){
		windowSize = $(window).width();
		if( swipeBreakPoint > windowSize ) {
			swipeThis = true;

		} else {
			swipeThis = false;
		}
	};

	function startUpScript() {

	}
	function setUI() {
		// Main nav UI
		$('nav li a').hover(function(){
			$( $(this).attr('href') ).toggleClass('show');
		});
		$('nav li a').click(function(){
			if(swipeThis) {
				$( $(this).attr('href') ).toggleClass('show');
			}
		});
		$('#gothage-index div ul li a').on('click', function(e){
			startUpViewer(false);
			var curClass = $(this).attr('href');
			$('#thumb-chart').addClass('hidden');
			setTimeout(function(){
				$('#thumb-chart').removeClass('hidden');
				$('a.thumbnail-image').each(function(){
						if(!$(this).hasClass(curClass)) { $(this).addClass('inactive'); }
						if($(this).hasClass(curClass) && $(this).hasClass('inactive')) { $(this).removeClass('inactive'); }
				});
				setThumbArray();
			},800);
			e.preventDefault();
		});

		// Hammertime UI
		$('body').on('click', '.tapnextimage', function(e){
			swipeNext(false);
		});
		$('body').on('click', '.tapprevimage', function(e){
			swipeNext(true);
		});
		$('body').on('click', '.tapcloseviewer', function(e){
			startUpViewer(false);
		});


		// ImageView UI
		ivContent.on('click', 'article', function(e){
			e.preventDefault();
			if(!swipeThis) {
				if($(this).hasClass('current-article')) {
					
					
					
					var theclass = $(e.target).attr('class');
					if(theclass === 'timeline-track' || theclass === 'timeline-thumb') {
						
						

					} else {
						startUpViewer(false);
					}
				} else {
					$('.current-article').removeClass('current-article');
					$(this).addClass('current-article');
					remapClasses();
					calculateMargin(true);
				}
			}
		});

		ivContent.on('mousedown', 'article', function(e) {
			e.preventDefault();
			if(!swipeThis) {
				if($(this).hasClass('current-article')) {
					var theclass = $(e.target).attr('class');
					if(theclass === 'timeline-track' || theclass === 'timeline-thumb') {
						timeDrag = true;

						updatebar(e.pageX);
					} else {
						
					}
				}
			}
		});
		$(document).mouseup(function(e) {
			$.each($('footer article'), function(index, val) {
				if($(this).hasClass('show')) {
					$(this).removeClass('show');
				}
			});
			if(timeDrag) {
				timeDrag = false;
				updatebar(e.pageX);
				console.log('mosueup -'+timeDrag);
			}
		});
		$(document).mousemove(function(e) {
			if(timeDrag) {
				updatebar(e.pageX);
			}
		});
	}

	function setVideoHandlers(vid) {
		theVideo = vid;
		theVideo.on('timeupdate', function(){
			var currentPos = theVideo[0].currentTime; //Get currenttime
			var maxduration = theVideo[0].duration; //Get theVideo duration
			var percentage = 100 * currentPos / maxduration; //in %
			$('.timeline-thumb').css('width', percentage+'%');
		});
	}
	

	setUI();
	// MENU FUNCTIONS
	


	// HANDLE CATEGORY SELECTION
	
	



	// SET SWIPE STUFF

	// IMAGE VIEWER START
	
	
	function setThumbArray(){
		var videoObject = [];
		thumbArray   = [];
		$('a.thumbnail-image').each(function(){
			if ( !$(this).hasClass('inactive') ) {
				var url = $(this).attr('href');
				if( $(this).attr('data-ogg') || $(this).attr('data-mp4') || $(this).attr('data-webm') ) {
					videoObject['ogg'] = $(this).attr('data-ogg');
					videoObject['mp4'] = $(this).attr('data-mp4');
					videoObject['webm'] = $(this).attr('data-webm');
					videoObject['ogghires'] = $(this).attr('data-ogghires');
					videoObject['mp4hires'] = $(this).attr('data-mp4hires');
					videoObject['webmhires'] = $(this).attr('data-webmhires');
					videoArray[url] = videoObject;
				}
				thumbArray.push(url);
				lastArray[url] = ( $(this).hasClass('add-space-right') ? 1 : 0 );
			}
		});
	}
	
	
	



	//update Progress Bar control
	var updatebar = function(x) {
		var progress = $('.timeline-track');
		var maxduration = theVideo[0].duration; //Video duraiton
		var position = x - progress.offset().left; //Click pos
		var percentage = 100 * position / progress.width();
		console.log(position);
		console.log('percentage: '+percentage);

		//Check within range
		if(percentage > 100) {
			percentage = 100;
		}
		if(percentage < 0) {
			percentage = 0;
		}
		console.log(percentage);
		//Update progress bar and video currenttime
		$('.timeBar').css('width', percentage+'%');
		theVideo[0].currentTime = maxduration * percentage / 100;
	};





	ivContent.on('timeupdate','article video', function() {
		
		var currentPos = video[0].currentTime; //Get currenttime
		var maxduration = video[0].duration; //Get video duration
		var percentage = 100 * currentPos / maxduration; //in %
		$('.timeline-thumb').css('width', percentage+'%');
	});

	ivContent.on('mousemove', '.videohover', function(e) {
		
		var vObj = $(this).children('video');
		
		if(!vObj.paused) {
			
			var pbtn = vObj.siblings('.pausebutton');
			pbtn.stop().fadeIn(0);
			window.clearTimeout(timer);
				var msBeforeFade = 600;
				timer = window.setTimeout(function(){
					pbtn.stop().fadeOut(msBeforeFade);
				},msBeforeFade);
		}
	});


	ivContent.on('mousestop', '.videohover', function(e) {
		var vObj = $(this).children('video');
		var pbtn = vObj.siblings('.pausebutton');
		if(!vObj.paused) {
			
		}
	});

	ivContent.on('ended', 'video', function(e){
		$(this).fadeOut('fast');
		$(this).siblings('.playbutton').fadeIn('fast');
		$('.videohover').removeClass('videohover');
	});
	ivContent.on('click', 'div', function(e){
		var vObj;
		if($(this).hasClass('playbutton') && !swipeThis) {
			if($(this).parent('article').hasClass('current-article')) {
				e.stopPropagation();
				vObj = $(this).siblings('video');
				vObj.css('display', 'block');
				vObj.fadeIn('fast');
				playCurrentVideo();
				$('.current-article').addClass('videohover');
			}
		}
	});
	
	ivContent.on('click', 'video, .pausebutton', function(e){
		if(!swipeThis) {
			if( $(this).parent('article').hasClass('prev-article') ) {
			} else if ( $(this).parent('article').hasClass('next-article') ) {
			} else if ( $(this).parent('article').hasClass('current-article') && $(this).get(0).paused ) {
			} else {
				e.stopPropagation();
				var vObj = $(this).parent('article').children('video');
				stopAllVideo();
				if($('.pausebutton')) { $('.pausebutton').addClass('playbutton').removeClass('pausebutton'); }
				$('.playbutton').fadeIn('fast');
				$('.videohover').removeClass('videohover');
			}
			
		}
	});
	


	/*
		Touch events checking
		--------------------- */
	var hammertime = Hammer(ivContent, {
		transform_always_block : true,
		transform_min_scale : 1,
		drag_block_horizontal: true,
		drag_block_vertical: false,
		drag_min_distance: 0
	});
	var posX=0, posY=0,lastPosX=0,lastPosY=0,bufferX=0,bufferY=0,scale=1,last_scale,rotation=1,last_rotation,dragReady=0,changePoint=windowSize/3;

	hammertime.on('touch drag dragend swipe tap', function(ev){
		ev.stopPropagation();
		ev.preventDefault();
		if(swipeThis) manageTouch(ev);
	});


	function manageTouch(ev) {
		ev.stopPropagation();
		$theTarget = ev.target;
		switch(ev.type) {

			case 'tap':
				if($theTarget.nodeName === 'DIV' || $theTarget.nodeName === 'VIDEO') {


					if( $theTarget.nodeName === 'DIV' ) {
						vObj = $('.current-article').children('video');
						vObj.css('display', 'box');
						vObj.fadeIn('fast');
						$(this).fadeOut('fast');
						playCurrentVideo();
					}
					
				} else {
					startUpReady = false;
					setTimeout(function(){ startUpReady = true;	} ,800);
					startUpViewer(false);
				}

				
				
			break;
			case 'drag':
				
				posX = ev.gesture.deltaX + lastPosX;
				
				ivContent.css('margin-left', ivContentMargin+posX);
			break;
			case 'dragend':
				if( posX>changePoint ) {
					swipeNext(true);
				} else if( posX < (changePoint*-1) ) {
					swipeNext(false);
				} else {
					ivContent.animate({'marginLeft': ivContentMargin},'fast');
				}
				
			break;
			case 'swipe':
			break;
		}
	}
	function swipeNext(next){
		var firstArticle, firstArticleWidth;
		stopAllVideo();
		firstArticle      = $('#iv-content article').first();
		lastArticle       = $('#iv-content article').last();
		firstArticleWidth = firstArticle.outerWidth();
		$('.current-article').removeClass('current-article');
		if(next){
			ivContent.animate({'marginLeft': 0},'fast', function() {
				firstArticle.addClass('current-article');
				insertArticle(getNewUrl(false), true);
				ivContent.css( 'marginLeft' , ivContentMargin );
				
				remapClasses();
				lastArticle.remove();
			});
			
		}else {
			ivContent.animate({'marginLeft': '-'+firstArticleWidth*2},'fast', function(){
				lastArticle.addClass('current-article');
				insertArticle(getNewUrl(true), false);
				ivContent.css( 'marginLeft' , ivContentMargin );
				
				remapClasses();
				firstArticle.remove();
			});
		}
		
	}


	function startUpViewer(really) {
		if(really) {
			
			ivWrapper.removeClass('closed');
			$('#thumb-chart').addClass('hidden');
			$('header img').addClass('hidden');
			if(swipeThis) {
				$('#thumb-chart').hide();
				$('header').hide();
				$('footer').hide();
				setTimeout(function(){
					// Hide the address bar!
					window.scrollTo(0, 1);
				}, 0);
			}
			if(swipeThis) {
				$('body').append('<div id="swipenav"><ul><li><a class="tapprevimage" href="#">Prev</a></li><li><a class="tapnextimage" href="#">Next</a></li><li><a class="tapcloseviewer" href="#">Close</a></li></ul></div>');
			}
		} else {
			if( $('#swipenav').length !== 0 ) {
				$('#swipenav').remove();
			}
			ivWrapper.addClass('closed');
			setTimeout(function(){ivContent.empty();}, 800);
			$('#thumb-chart').removeClass('hidden');
			$('header img').removeClass('hidden');
			if (swipeThis) {
				setTimeout(function(){
					// $('#thumb-chart').removeClass('hidden');
					$('#thumb-chart').fadeIn('fast');
					ivContent.removeAttr('style');
				},300);
				$('header').show();
				$('footer').show();
			}
		}
	}
	
	$('.cat-holder a').click(function(e){
		var currentUrl = $(this).attr('href');
		var indexesToLoop = [];
		e.preventDefault();
		if(startUpReady){
			indexesToLoop.push($.inArray(currentUrl, thumbArray));
			indexesToLoop.push( ( thumbArray[indexesToLoop[0]-1] ? $.inArray(thumbArray[indexesToLoop[0]-1], thumbArray) : $.inArray(thumbArray[thumbArray.length -1], thumbArray) ) );
			indexesToLoop.push( ( thumbArray[indexesToLoop[0]+1] ? $.inArray(thumbArray[indexesToLoop[0]+1], thumbArray) : $.inArray(thumbArray[0], thumbArray) ) );
			for ( var i = 0; i < indexesToLoop.length; i++ ) {
				var classes,thisUrl,imtlPrepends;
				thisUrl = thumbArray[indexesToLoop[i]];
				imtlPrepends = false;
				if(i === 0) {
					classes = 'current-article';
				} else if (i == 1) {
					classes = 'prev-article';
					imtlPrepends = true;
				} else if (i == 2) {
					classes = 'next-article';
				}
				if(lastArray[thisUrl]) { classes+= ' last'; }
				var imgObjtoLoad = createToLoadObject(thisUrl, imtlPrepends, classes);
				imagesToLoad.push(imgObjtoLoad);
			}
			startUpViewer(true);
			loadArticles();
		}
	});

	function showLoader() {
		if( !$('#loading').hasClass('show')  ) {$('#loading').addClass('show');}
	}
	function hideLoader() {
		if( $('#loading').hasClass('show')  ) {$('#loading').removeClass('show');}
	}

	// CREATING OBJECT TO BE LOADED IN QUEUE
	function createToLoadObject(thumburl, imtlPrepends, classString) {
		var obj = {
			url: thumburl,
			imtlPrepends: imtlPrepends,
			classes: classString
		};
		return obj;
	}
	function insertArticle(thumburl, imtlPrepends, classString) {
		
		var appendString, controls, poster, setheight;
		appendString = '<article class="'+classString+' hidden" >';
		var $theobject;
		poster = ' poster="'+thumburl+'"';

		if(swipeThis) {
			appendString+='<div class="vertical-helper"></div>';
			if( videoArray[thumburl] ) {
				setheight = $(window).width() * 0.56;
				controls = ' controls';
				controls+=poster;
			}
		} else {
			if( videoArray[thumburl] ) {
				controls+=poster;
			}
		}
		
		if(videoArray[thumburl]) {
			

			appendString+= '<video class="video"'+controls+'>';
			if(swipeThis) {
				appendString+= '<source src="'+videoArray[thumburl]['webm']+'" type="video/webm" />';
				appendString+= '<source src="'+videoArray[thumburl]['ogg']+'" type="video/ogg" />';
				appendString+= '<source src="'+videoArray[thumburl]['mp4']+'" type="video/mp4" />';
			} else {
				appendString+= '<source src="'+videoArray[thumburl]['webmhires']+'" type="video/webm" />';
				appendString+= '<source src="'+videoArray[thumburl]['ogghires']+'" type="video/ogg" />';
				appendString+= '<source src="'+videoArray[thumburl]['mp4hires']+'" type="video/mp4" />';
			}
			

			appendString+= '</video>';
			if(!swipeThis){

				appendString+= '<div class="playbutton"></div>';
				appendString+= '<div class="timeline-track"><div class="timeline-thumb"></div></div>';
			}
			
		}else{
			
			appendString+= '<img src="'+thumburl+'"/>';
		}
		appendString += '</article>';
		$theobject = $(appendString);
		if(imtlPrepends) {
			ivContent.prepend(appendString);
		} else  {
			ivContent.append(appendString);
		}
		
		if(videoArray[thumburl]) {
			var arr = classString.split(' ');
			setVideoHandlers( $('.'+arr[0]+' video') );
		}
		
		setTimeout(function(){
			$('#iv-content article.hidden').removeClass('hidden');
			
		}, 100);
		if(!swipeThis) {
			$('#iv-content article video').each(function(){
				var ratioMultiplier = 1.77777777778;
				var thewidth = $(this).height()*ratioMultiplier;
				$(this).css('width', thewidth+16+'px');
			});
			$('#iv-content article img').each(function(){
				$(this).parent('article').css('width', $(this).width()+16+'px' );
				// $(this).css('width', $(this).children('img').width()+16+'px');
			});
			// $('#iv-content article').each(function(){
			// 	console.log($(this).children('img').width());
			// 	if($(this).children('video')) {
					
					
					
			// 	} else {
			// 		// $(this).css('width', $(this).children('img').width()+16+'px');
			// 	}
				
			// });
		}
		
		calculateMargin(false);
	}

	function remapClasses() {
		var classToAdd = 'prev-article';
		ivContent.children('article').each(function(i){
			if (!$(this).hasClass('current-article')) {
				$(this).removeClass('prev-article').removeClass('next-article');
				$(this).addClass(classToAdd);
			}
			if ($(this).hasClass('current-article')) {
				$('.current-article').removeClass('current-article');
				$(this).addClass('current-article');
				$(this).removeClass('prev-article').removeClass('next-article');
				classToAdd = 'next-article';
			}
		});
	}
	function loadArticles(){
		if(!imagesToLoad[0]){
			ivContentMargin = parseInt(ivContent.css('marginLeft'), 10);
			hideLoader();
			checkForSpace();
			return;
		}else{
			showLoader();
			var url = imagesToLoad[0].url;
			var imgObj = new Image();
			imgObj.onload = function() {
				insertArticle(imagesToLoad[0].url, imagesToLoad[0].imtlPrepends, imagesToLoad[0].classes);
				imagesToLoad.shift();
				loadArticles();
			};
			imgObj.src = url;
		}
	}
	function stopAllVideo() {
		window.clearTimeout(timer);
		$('video').each(function(){
			$(this).get(0).pause();
			$('.videohover').removeClass('videohover');
			$('.pausebutton').addClass('playbutton').removeClass('pausebutton');
			if(!swipeThis) {
				$(this).siblings('.playbutton').fadeIn('fast');
			}
		});
	}
	function playCurrentVideo() {

		var vObj = $('.current-article').children('video');
		
		$('.playbutton').removeClass('playbutton').addClass('pausebutton').fadeIn(0);
		if(!swipeThis) {
			$('.timeline-track').show();
		}
		
		vObj[0].play();

	}
	function calculateMargin(animate) {
		var includeWidth = true;
		var newMargin = 0;
		var contentWidth = 0;
		var wrapperWidth = ivWrapper.width()/2;
		ivContent.children('article').each(function(i){
			if(includeWidth) {
				if (!$(this).hasClass('current-article')) {
					contentWidth += $(this).outerWidth();
					contentWidth += parseInt($(this).css('marginRight'), 10);
				}
				if ($(this).hasClass('current-article')) {
					contentWidth+=$(this).width()/2;
					includeWidth = false;
				}
			}
		});
		newMargin = wrapperWidth-contentWidth;
		if(animate) { stopAllVideo(); ivContent.stop().animate({'marginLeft' : newMargin}, 500, function(){checkForSpace();}); }
		else {
			ivContent.css('margin-left',newMargin+'px');
		}
	}
	function getNewUrl(getNext) {
		var thisUrl      = '';
		var theURL       = '';
		var firstArticle = $('#iv-content article').first();
		var lastArticle  = $('#iv-content article').last();

		if(getNext) {
			thisUrl = lastArticle.find('img').attr('src');
			if(thisUrl === undefined) {
				thisUrl = lastArticle.find('video').attr('poster');
				
			}

			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex+1] ? $.inArray(thumbArray[getIndex+1], thumbArray) : $.inArray( thumbArray[0], thumbArray ) );
			theURL = thumbArray[getIndex];
		} else {
			thisUrl = firstArticle.find('img').attr('src');
			if(thisUrl === undefined) {
				thisUrl = firstArticle.find('video').attr('poster');
				
			}
			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex-1] ? $.inArray(thumbArray[getIndex-1], thumbArray) : $.inArray( thumbArray[thumbArray.length-1], thumbArray ) );
			theURL = thumbArray[getIndex];
		}
		return theURL;
	}
	function checkForSpace() {
		if( !swipeThis ) {
			
			var firstImage,lastImage,contentLeftMargin,firstArticleWidth,firstArticle,lastArticle,lastArticleOffset,thisUrl,getIndex,theURL,classes,imgObjtoLoad,getLoading;
			firstArticle      = $('#iv-content article').first();
			lastArticle       = $('#iv-content article').last();
			firstArticleWidth = firstArticle.outerWidth();
			lastArticleOffset = lastArticle.offset();
			contentLeftMargin = parseInt(ivContent.css('marginLeft'), 10);
			getLoading = false;
			
			if( contentLeftMargin < firstArticleWidth*-1 ) {
				$('#iv-content article').first().remove();
				calculateMargin();
			}
			
			if( contentLeftMargin > 0 ) {
				theURL = getNewUrl(false);
				classes = 'prev-article';
				if(lastArray[theURL]) { classes += ' last';}
				imgObjtoLoad = createToLoadObject(theURL, true, classes);
				imagesToLoad.push(imgObjtoLoad);
				getLoading = true;
			}
			
			if(lastArticleOffset.left+lastArticle.innerWidth() < ivWrapper.innerWidth()) {
				
				theURL = getNewUrl(true);
				classes = 'next-article';
				if(lastArray[theURL]) { classes += ' last';}
				imgObjtoLoad = createToLoadObject(theURL, false, classes);
				imagesToLoad.push(imgObjtoLoad);
				getLoading = true;
			}
			
			if(lastArticleOffset.left > $(document).width()) {
				lastArticle.remove();
			}
			
			if(getLoading && !ivWrapper.hasClass('closed')) {
				loadArticles();
			}
		} else {
			//- UNDER SWIPE BREAKPOINT


		}
	}
	removeHidden($('#intrologo'));
	$('#intrologo').click(function(){
		if($('#thumb-chart').hasClass('hidden')) {
			$('#thumb-chart').removeClass('hidden');
		}
		if($('header').hasClass('hidden')) {
			$('header').removeClass('hidden');
		}
		if($('footer').hasClass('hidden')) {
			$('footer').removeClass('hidden');
		}
		$(this).fadeOut('fast', function() {
			$(this).remove();
		});
	});

	

	setTimeout( function() {
		removeHidden($('#thumb-chart'));
	}, 2000 );
	setTimeout( function() {
		removeHidden($('header'));
		$('#intrologo').addClass('hidden');
	}, 2500 );
	setTimeout( function() {
		removeHidden($('footer'));
		$('#intrologo').fadeOut('fast',function(){ $('#intrologo').remove(); });
	}, 3000 );

	function removeHidden(obj) {
		if(obj.hasClass('hidden')) {
			obj.removeClass('hidden');
		}
	}



			var defaults = {
						upBox		: 'header',
						downBox		: 'nav',
						speed		: 0,
						speedMod	: 0.05,
						maxSpeed	: 10,
						stopChildren: true
					};
			var $this, _ub, _db, _sc, _speed, _speedMod, _maxSpeed, _d, scrolling, _st;
			scrolling = null;
			_d			= document.documentElement;
			
			_ub			= defaults.upBox;
			_db			= defaults.downBox;
			_speed		= defaults.speed;
			_speedMod	= defaults.speedMod;
			_maxSpeed	= defaults.maxSpeed;
			_st			= defaults.stopChildren;
			
			if (navigator.userAgent.indexOf("Firefox")!=-1) {_d = document.body.parentNode;}
			function scrollThis(i){
				if(_speed >= _maxSpeed) {_speed = _maxSpeed;}
				else {_speed += _speedMod;}
				if(i === _ub) {_d.scrollTop -= _speed;}
				else  {_d.scrollTop += _speed;}
				scrolling = window.setTimeout(function(){scrollThis(i);}, 5);
			}
			function stopScroll() {	_speed = 0; window.clearTimeout(scrolling);}
			
			$(_ub+','+_db).hover(function(){
				// alert($(this)[0].nodeName.toLowerCase());
					scrollThis($(this)[0].tagName.toLowerCase());
				},function(){
					stopScroll();
				}
			);
			if(_st) {
				$(_ub).children().mousemove(function(){stopScroll();});
				$(_db).children().mousemove(function(){stopScroll();});
			}

});