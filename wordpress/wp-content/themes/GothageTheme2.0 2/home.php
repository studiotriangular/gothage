<?php
/*Template Name: frontpage */ 
	
?>

<?php get_header(); ?>
<a id="intrologo" class="hidden" href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logoblack.png" alt="Mikael Göthage Photographs"/></a>
<?php get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order')); ?>
<div id="thumb-chart" class="hidden">
<?php /* BEGIN HANDLING LOOPS */ 


foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $value, 'order_by' => 'name', 'order'=>'ASC'); 
		query_posts($new_args);?>	
		<?php if(have_posts()): ?> 
			<div class="cat-holder">
				<?php while(have_posts()) : the_post();?>
					<?php if(has_post_thumbnail()) :
							$terms = get_the_terms( $post->ID, 'pwtist_tag' ); 
							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');?>
							
							<?php // BUILD IMAGE LINK

							$a_classes = 'thumbnail-image overview';
							if( get_post_meta($post->ID, 'mp4', true) ) {
								$a_classes.= ' videopost';
							}
							// Get video meta 
							$ogg = get_post_meta( $post->ID, 'ogg', true );
							$mp4 = get_post_meta( $post->ID, 'mp4', true );
							$webm = get_post_meta( $post->ID, 'webm', true );

							$ogghires = get_post_meta( $post->ID, 'ogg-hires', true );
							$mp4hires = get_post_meta( $post->ID, 'mp4-hires', true );
							$webmhires = get_post_meta( $post->ID, 'webm-hires', true );

							foreach($terms as $term) {$a_classes .= ' ' . $term->slug;}
							if($wp_query->current_post == $wp_query->post_count-1) {$a_classes .= ' add-space-right';}
							$videodatastring = 'data-ogg="'.$ogg.'" data-mp4="'.$mp4.'" data-webm="'.$webm.'"'.' data-ogghires="'.$ogghires.'" data-mp4hires="'.$mp4hires.'" data-webmhires="'.$webmhries.'"';
							$a_link = '<a '.$videodatastring.' href="'.$full_image_url[0].'" class="'.$a_classes.'">';
							echo $a_link; ?>
							<?php the_post_thumbnail('thumbnail'); ?>
							
							
							 </a>	
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		<?php endif; wp_reset_query(); ?>
	<?php endforeach; ?>
<?php endforeach; ?>
</div><!-- thumb-chart end -->


<div id="iv-wrapper" class="closed"><div id="iv-content"></div></div>


<?php get_footer(); ?>






