$(document).ready(function(){
	var swipeBreakPoint = 1024;
	var windowSize = $(window).width();
	
	window.onresize = function(e){
		windowSize = $(window).width();
	};


	// MENU FUNCTIONS
	$('nav li a').hover(function(){
		toggleArticle($(this).attr('href'));
	});
	function toggleArticle(string){
		$(string).toggleClass('show');
	}

	// HANDLE CATEGORY SELECTION
	var fade = true;
	$('#gothage-index div ul li a').on('click', function(e){
		var curClass = $(this).attr('href');
		$('#thumb-chart').addClass('hidden');
		setTimeout(function(){
			$('#thumb-chart').removeClass('hidden');
			$('a.thumbnail-image').each(function(){
					if(!$(this).hasClass(curClass)) { $(this).addClass('inactive'); }
					if($(this).hasClass(curClass) && $(this).hasClass('inactive')) { $(this).removeClass('inactive'); }
			});
			setThumbArray();
		},800);
		e.preventDefault();
		
		
	});



	// SET SWIPE STUFF

	// IMAGE VIEWER START
	var thumbArray, lastArray, videoArray, imagesToLoad;
	ivWrapper    = $('#iv-wrapper');
	ivContent    = $('#iv-content');
	
	lastArray    = [];
	videoArray   = [];
	imagesToLoad = [];




	setThumbArray();
	function collectSwipes(){
		var thisUrl = '';
		var swipeObjects = '<div class="swipe-wrap">';
		$('a.thumbnail-image').each(function(){
			if ( !$(this).hasClass('inactive') ) {
				thisUrl = $(this).attr('href');
				thisUrl = $(this).children('img').attr('src');
				swipeObjects += '<div>';
				if( $(this).attr('data-ogg') && $(this).attr('data-mp4') && $(this).attr('data-webm') ) {
					swipeObjects+= '<video class="video" style="display:none; position:relative; height:100%; width:auto;">';
					swipeObjects+= '<source src="'+$(this).attr('data-ogg')+'" type="video/webm; codecs=\'vp8,vorbis\'" />';
					swipeObjects+= '<source src="'+$(this).attr('data-mp4')+'" type="video/ogg; codecs=\'theora, vorbis\'" />';
					swipeObjects+= '<source src="'+$(this).attr('data-webm')+'" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'" />';
					swipeObjects+= '</video>';
					swipeObjects+= '<div class="playbutton"></div>';
				}
				swipeObjects += '<img src="'+thisUrl+'" />';
				swipeObjects += '</div>';
			}
		});
		swipeObjects+='</div>';
		return swipeObjects;
	}
	function setThumbArray(){
		var videoObject = [];
		thumbArray   = [];
		$('a.thumbnail-image').each(function(){
			if ( !$(this).hasClass('inactive') ) {
				var url = $(this).attr('href');
				if( $(this).attr('data-ogg') && $(this).attr('data-mp4') && $(this).attr('data-webm') ) {
					videoObject['ogg'] = $(this).attr('data-ogg');
					videoObject['mp4'] = $(this).attr('data-mp4');
					videoObject['webm'] = $(this).attr('data-webm');
					videoArray[url] = videoObject;
				}
				thumbArray.push(url);
				lastArray[url] = ( $(this).hasClass('add-space-right') ? 1 : 0 );
			}
		});
	}
	

	ivContent.on('click', 'article', function(e){
		if($(this).hasClass('current-article')) {
			startUpViewer(false);
		} else {
			$('.current-article').removeClass('current-article');
			$(this).addClass('current-article');
			remapClasses();
			calculateMargin(true);
		}
	});
	ivContent.on('ended', 'video', function(e){
		$(this).fadeOut('fast');
		$(this).siblings('.playbutton').fadeIn('fast');
	});
	ivContent.on('click', 'div', function(e){
		var vObj;
		if($(this).hasClass('playbutton')) {
			vObj = $(this).siblings('video');
			vObj.css('display', 'block');
			e.stopPropagation();
			vObj.fadeIn('fast');
			$(this).fadeOut('fast');
			vObj[0].play();
			
			
		}
		
	});
	
	ivContent.on('click', 'video', function(e){
		e.stopPropagation();
		$(this).fadeOut('fast');
		
		$(this).get(0).pause();

		$(this).siblings('.playbutton').fadeIn('fast');
	});
	

	function startUpViewer(really) {
		if(really) {
			ivWrapper.removeClass('closed');
			$('#thumb-chart').addClass('hidden');
			$('header img').addClass('hidden');
		} else {
			ivWrapper.addClass('closed');
			setTimeout(function(){ivContent.empty();}, 800);
			$('#thumb-chart').removeClass('hidden');
			$('header img').removeClass('hidden');
		}
	}
	
	$('.cat-holder a').click(function(e){
		var currentUrl = $(this).attr('href');
		var indexesToLoop = [];
		e.preventDefault();
		if(windowSize > swipeBreakPoint) {
			indexesToLoop.push($.inArray(currentUrl, thumbArray));
			indexesToLoop.push( ( thumbArray[indexesToLoop[0]-1] ? $.inArray(thumbArray[indexesToLoop[0]-1], thumbArray) : $.inArray(thumbArray[thumbArray.length -1], thumbArray) ) );
			indexesToLoop.push( ( thumbArray[indexesToLoop[0]+1] ? $.inArray(thumbArray[indexesToLoop[0]+1], thumbArray) : $.inArray(thumbArray[0], thumbArray) ) );
			for ( var i = 0; i < indexesToLoop.length; i++ ) {
				var classes,thisUrl,imtlPrepends;
				thisUrl = thumbArray[indexesToLoop[i]];
				imtlPrepends = false;
				if(i === 0) {
					classes = 'current-article';
				} else if (i == 1) {
					classes = 'prev-article';
					imtlPrepends = true;
				} else if (i == 2) {
					classes = 'next-article';
				}
				if(lastArray[thisUrl]) { classes+= ' last'; }
				var imgObjtoLoad = createToLoadObject(thisUrl, imtlPrepends, classes);
				imagesToLoad.push(imgObjtoLoad);
			}
			startUpViewer(true);
			loadArticles();
		} else {

			$('#thumb-chart').hide();
			$('#mySwipe').empty().append(collectSwipes);
			var elem = document.getElementById('mySwipe');
			window.mySwipe = Swipe(elem, {
				startSlide: $.inArray(currentUrl, thumbArray)
				// startSlide: 4,
				// auto: 3000,
				// continuous: true,
				// disableScroll: true,
				// stopPropagation: true,
				// callback: function(index, element) {},
				// transitionEnd: function(index, element) {}
			});
			
		}
		
		
		
	});




	// CREATING OBJECT TO BE LOADED IN QUEUE
	function createToLoadObject(thumburl, imtlPrepends, classString) {
		var obj = {
			url: thumburl,
			imtlPrepends: imtlPrepends,
			classes: classString
		};
		return obj;
	}
	function insertArticle(thumburl, imtlPrepends, classString) {
		var appendString = '<article class="'+classString+' hidden">';
		if(videoArray[thumburl]) {
			appendString+= '<video class="video" style="display:none; position:relative; height:100%; width:auto;">';
			
			appendString+= '<source src="'+videoArray[thumburl]['webm']+'" type="video/webm; codecs=\'vp8,vorbis\'" />';
			appendString+= '<source src="'+videoArray[thumburl]['ogg']+'" type="video/ogg; codecs=\'theora, vorbis\'" />';
			appendString+= '<source src="'+videoArray[thumburl]['mp4']+'" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'" />';

			appendString+= '</video>';
			appendString+= '<div class="playbutton"></div>';
		}else{
			
			
		}
		appendString+= '<img src="'+thumburl+'"/>';
		appendString += '</article>';
		if(imtlPrepends) {
			ivContent.prepend(appendString);
		} else  {
			ivContent.append(appendString);
		}
		setTimeout(function(){
			$('#iv-content article.hidden').removeClass('hidden');
		}, 100);
		calculateMargin(false);
	}

	function remapClasses() {
		var classToAdd = 'prev-article';
		ivContent.children('article').each(function(i){
			if (!$(this).hasClass('current-article')) {
				$(this).removeClass('prev-article').removeClass('next-article');
				$(this).addClass(classToAdd);
			}
			
			if ($(this).hasClass('current-article')) {
				$(this).removeClass('prev-article').removeClass('next-article');
				classToAdd = 'next-article';
			}
		});
	}

	function loadArticles(){
		if(!imagesToLoad[0]){
			$('#loading').removeClass('show');
			checkForSpace();
			return;
		}else{
			$('#loading').addClass('show');
			var url = imagesToLoad[0].url;
			var imgObj = new Image();
			imgObj.onload = function() {
				insertArticle(imagesToLoad[0].url, imagesToLoad[0].imtlPrepends, imagesToLoad[0].classes);
				imagesToLoad.shift();
				loadArticles();
			};
			imgObj.src = url;
		}
	}

	function calculateMargin(animate) {
		var includeWidth = true;
		var newMargin = 0;
		var contentWidth = 0;
		var wrapperWidth = ivWrapper.width()/2;
		ivContent.children('article').each(function(i){
			if(includeWidth) {
				if (!$(this).hasClass('current-article')) {
					contentWidth += $(this).outerWidth();
					contentWidth += parseInt($(this).css('marginRight'), 10);
				}
				if ($(this).hasClass('current-article')) {
					contentWidth+=$(this).width()/2;
					includeWidth = false;
				}
			}
		});
		newMargin = wrapperWidth-contentWidth;
		if(animate) { ivContent.stop().animate({'marginLeft' : newMargin}, 500, function(){checkForSpace();}); }
		else {
			ivContent.css('margin-left',newMargin+'px');
		}
	}
	function checkForSpace() {
		var firstImage,lastImage,contentLeftMargin,firstArticleWidth,firstArticle,lastArticle,lastArticleOffset,thisUrl,getIndex,theURL,classes,imgObjtoLoad,getLoading;
		firstArticle      = $('#iv-content article').first();
		lastArticle       = $('#iv-content article').last();
		firstArticleWidth = firstArticle.outerWidth();
		lastArticleOffset = lastArticle.offset();
		contentLeftMargin = parseInt(ivContent.css('marginLeft'), 10);
		getLoading = false;
		if( contentLeftMargin < firstArticleWidth*-1 ) {
			$('#iv-content article').first().remove();
			calculateMargin();
		}
		if( contentLeftMargin > 0 ) {
			thisUrl = firstArticle.children('img').attr('src');
			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex-1] ? $.inArray(thumbArray[getIndex-1], thumbArray) : $.inArray( thumbArray[thumbArray.length-1], thumbArray ) );
			theURL = thumbArray[getIndex];
			classes = 'prev-article';
			if(lastArray[theURL]) { classes += ' last';}
			imgObjtoLoad = createToLoadObject(theURL, true, classes);
			imagesToLoad.push(imgObjtoLoad);
			getLoading = true;
		}
		if(lastArticleOffset.left+lastArticle.innerWidth() < ivWrapper.innerWidth()) {
			thisUrl = lastArticle.children('img').attr('src');
			getIndex = $.inArray(thisUrl, thumbArray);
			getIndex = ( thumbArray[getIndex+1] ? $.inArray(thumbArray[getIndex+1], thumbArray) : $.inArray( thumbArray[0], thumbArray ) );
			theURL = thumbArray[getIndex];
			classes = 'next-article';
			if(lastArray[theURL]) { classes += ' last';}
			imgObjtoLoad = createToLoadObject(theURL, false, classes);
			imagesToLoad.push(imgObjtoLoad);
			getLoading = true;
		}
		if(lastArticleOffset.left > $(document).width()) {
			lastArticle.remove();
		}
		if(getLoading && !ivWrapper.hasClass('closed')) {
			loadArticles();
		}
		
	}
});