<?php 


function go_maintenance_mode() {
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
        wp_die('Sorry, but we are doing Maintenance on the site, please check back soon.');
    }
}
// add_action('get_header', 'go_maintenance_mode');

add_action('wp_enqueue_scripts', 'mmm_scripts_and_styles');
add_action( 'init', 'initfuncs' );
add_action('widgets_init', 'registersidebarsh2h');
add_theme_support('post-thumbnails');




function initfuncs() {
	
	register_nav_menus(
		array(
			'main_nav' => 'Main Nav',
		)
	);
}
function mmm_scripts_and_styles() {
  
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  

    wp_deregister_script('jquery');

    wp_register_script( 'topscript', get_stylesheet_directory_uri().'/foundation/javascripts/concat/top.js');
    wp_register_script( 'bottomscript', get_stylesheet_directory_uri() . '/foundation/javascripts/concat/bottom.js', array( 'topscript' ), '', true );
    
    // wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', false, '1.6.1');
    
    // wp_enqueue_script( 'jquery' );
    
    wp_enqueue_script( 'topscript' );
    wp_enqueue_script( 'bottomscript' );

    // styles
    wp_enqueue_style( 'main-style', get_stylesheet_uri() );
    wp_enqueue_style( 'sassy', get_stylesheet_directory_uri().'/foundation/stylesheets/app.css' );
  
}


function registersidebarsh2h(){
	 register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
}


