<?php /*Template Name: frontpage */ ?>

<?php get_header(); ?>
<?php 
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order'));

?>
<!-- index -->
<div id="thumb-chart">
<?php /* BEGIN HANDLING LOOPS */ 

foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $value); 
		query_posts($new_args);?>
			
		<?php if(have_posts()): 
			?><div class="cat-holder"><?php
		while(have_posts()) : the_post();?>
		
			<?php
			if(get_post_meta($post->ID, 'pwtist_image_cap', true))	 
			{$pwtist_info 	= get_post_meta($post->ID, 'pwtist_image_cap', true);} ?>
			<?php if(has_post_thumbnail()) :
					$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
					<a class="thumbnail-image <?php echo $padClass; if($wp_query->current_post == $wp_query->post_count-1)echo "add-space-right"?>" alt="<?php /*echo $pwtist_info;*/ ?>" href="<?php echo $full_image_url[0]; ?>">
						 <?php 
						 if($isIOS) {
						 	the_post_thumbnail('full');
					 	}else {
					 		the_post_thumbnail('thumbnail'); 
					 	} ?> 
					 </a>		
			<?php endif; ?>
		<?php endwhile;?><div><?php endif;
		
	endforeach;
endforeach;
?>




</div><!-- thumb-chart end -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>