
<?php get_header(); ?>

<?php global $query_string;
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order')); ?>
<div id="thumb-chart">
<?php /* BEGIN HANDLING LOOPS */ 
foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
		$new_args = $query_string . '&pwtist_cat='.$value.'&order_by=name&order=ASC'; 
		query_posts($new_args);?>
		<?php if(have_posts()): ?> 
			<div class="cat-holder">
				<?php while(have_posts()) : the_post();?>
					<?php if(has_post_thumbnail()) :
							$terms = get_the_terms( $post->ID, 'pwtist_tag' ); 
							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
							<?php if($GLOBALS['isIOS']) {
								$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
								if ($GLOBALS['isPhonePod']) {
									$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
								}
							} ?>
							<?php // BUILD IMAGE LINK
							
							$a_classes = 'thumbnail-image overview';
							foreach($terms as $term) {$a_classes .= ' ' . $term->slug;}
							if($wp_query->current_post == $wp_query->post_count-1) {$a_classes .= ' add-space-right';}
							$a_link = '<a href="'.$full_image_url[0].'" class="'.$a_classes.'">';
							echo $a_link; ?>
							<?php if ($GLOBALS['isIOS']): ?>
								<?php if ($GLOBALS['isPhonePod']): ?>
									<?php the_post_thumbnail('thumbnail'); ?> 
								<?php else: ?>
									<?php the_post_thumbnail('medium'); ?> 
								<?php endif; ?>
								
							<?php else : ?>
								<?php the_post_thumbnail('thumbnail'); ?>
							<?php endif; ?>
								 
							 </a>	
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		<?php endif;wp_reset_query(); ?>
	<?php endforeach;?>
<?php endforeach; ?>
</div><!-- thumb-chart end -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>


