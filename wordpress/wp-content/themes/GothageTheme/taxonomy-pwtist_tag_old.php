
<?php get_header(); ?>
<h3 class="photo_set_display">Mikael Götehage Photographs Tag: <?php single_tag_title(); ?></h3>
<?php global $query_string;
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order')); ?>
<div id="thumb-chart">
<?php /* BEGIN HANDLING LOOPS */ 
foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
		$new_args = $query_string . '&pwtist_cat='.$value.'&order_by=name&order=ASC'; 
		query_posts($new_args);?>
		<?php if(have_posts()): ?> 
			<div class="cat-holder">
				<?php while(have_posts()) : the_post();?>
			
					<?php if(has_post_thumbnail()) :
							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
							<a class="thumbnail-image <?php if($wp_query->current_post == $wp_query->post_count-1)echo "add-space-right"?>" href="<?php echo $full_image_url[0]; ?>"> <?php the_post_thumbnail('thumbnail'); ?> </a>		
					<?php endif; //end if has post thumb ?> 
			
				<?php endwhile; //end while have posts ?> 
			</div>
		<?php endif; wp_reset_query(); //end if has posts  ?> 
	<?php endforeach;?>
<?php endforeach; ?>
</div><!-- thumb-chart end -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>


