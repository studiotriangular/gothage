(function($) {
	/*
	Auto Scroller
	Scroll window up or down automatically on hover.
	*/
	$.fn.autoScroll = function(options) {
		var defaults = {
			upBox 		: '.scroll-up',
			downBox 	: '.scroll-down',
			speed 		: 0,
			speedMod 	: 0.7,
			maxSpeed 	: 12,
			stopChildren: true				// If hover on children, stop scrolling?
		};
		if(options) {$.extend(defaults, options);}
		this.each(function(){
			var $this, _ub, _db, _sc, _speed, _speedMod, _maxSpeed, _d, scrolling, _st;
			scrolling = null;
			_d 			= document.body;
			
			_ub 		= defaults.upBox;
			_db 		= defaults.downBox;
			_speed 		= defaults.speed;
			_speedMod 	= defaults.speedMod;
			_maxSpeed 	= defaults.maxSpeed;
			_st 		= defaults.stopChildren;
			
			if (navigator.userAgent.indexOf("Firefox")!=-1) {_d = document.body.parentNode;}
			
			function scrollThis(i){
				if(_speed >= _maxSpeed) {_speed = _maxSpeed}
				else {_speed += _speedMod;}
				if(i === _ub) {_d.scrollTop -= _speed;}
				else  {_d.scrollTop += _speed;}
				scrolling = window.setTimeout(function(){scrollThis(i)}, 5);
			}
			function stopScroll() {	window.clearTimeout(scrolling);	speed = 0; }
			
			$(_ub+','+_db).hover(function(){
					scrollThis('#'+$(this).attr('id'));
				},function(){
					stopScroll();
				}
			);
			if(_st) {
				$(_ub).children().mousemove(function(){stopScroll();});
				$(_db).children().mousemove(function(){stopScroll();});
			}
		});
		return this;
	};
})(jQuery); 
