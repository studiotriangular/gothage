(function($) {
	/*
	Autoscroll div to read text 
	*/
	$.fn.slowscroll = function(options) {
		var defaults = {
			classToHover 	: '.hover',
			speed 			: 5000,
			delayer			: 3000
		}
		
		if(options) {$.extend(defaults, options)};
		this.each(function(){
			var $this, _cth, _s, _d, dif, origins;
			$this 	= $(this);
			_cth	= defaults.classToHover;
			_s		= defaults.speed;
			_d		= defaults.delayer;
			
			if($this.height() > $this.parent().height()) {
				dif = $this.height() - $this.parent().height();
				origins = parseInt($this.css('margin-top'), 10);
				$(_cth).hover(function(){
						$this.css('margin-top', origins);
						$this.stop().delay(_d).animate({'margin-top' : -dif}, _s);
					},function(){
						$this.stop().animate({'margin-top' : origins}, 'slow');
					}
				
				);	
			}
			
		});
		return this;
	};
})(jQuery);