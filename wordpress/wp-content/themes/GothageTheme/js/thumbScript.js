(function($){
	$.fn.thumbScript=function(options){
		var defaults={caption:true, def_class:'overview'};
		if(options){$.extend(defaults,options);}
		this.each(function(){
			var $this,$loading,$urlArray,$thumbArray,$lastArray,$imagesToLoad,$imagesToInsert,$nextAction,$wrapper,$content,$curClass;
			$this=$(this);
			$wrapper=$('#iv-wrapper');
			$content=$('#iv-content');
			$loading=false;
			$urlArray=[];
			$thumbArray=[];
			$lastArray=[];
			$imagesToLoad=[];
			$imagesToInsert=[];
			$nextAction=[];
			$curClass = defaults.def_class;
			
			function fadeInImage(i){i.css('opacity',0);i.fadeTo('slow',0.3,function(){i.css('opacity','');});}
			function hideLoader(i){if(i){$loading=false;$('#loading').fadeOut('fast');}else{$loading=true;$('#loading').fadeIn('fast');}}
			function exNext(){if($nextAction[0]){var na=$nextAction.shift();na();}}
			function getOpWidth(i){
				var $opWidth,$curImage,$pos;
				if(i){
					$curImage=$('.next-image');
				}else{
					$curImage=$('.current-image');
				}
				$pos=$('.current-image').position();
				if($pos===null){
					$opWidth=$curImage.width()/2;
				}else{
					$opWidth=$pos.left+($curImage.width()/2);
				}
				$opWidth=($wrapper.width()/2)-$opWidth;
				return $opWidth;
			}
			function loadQeue(){
				if(!$imagesToLoad[0]){
					hideLoader(true);
					exNext();
					return;
				}else{
					hideLoader(false);
					var $url,$img;
					$url=$imagesToLoad[0];
					$img=$('<img />').attr('src',$url).load(function(){
						if(!this.complete||typeof this.naturalWidth==="undefined"||this.naturalWidth===0){
							alert('Image broken.');
						}else{
							$urlArray[$url]=$img;
							$imagesToLoad.shift();
							loadQeue();
						}
					});
				}
			}
			function checkImage(url){
				if($urlArray[url]){
					return true;
				}else{
					$imagesToLoad.push(url);
					if(!$loading){
						loadQeue();
					}
					return false;
				}
			}
			function insertImages(){
				while($imagesToInsert[0]){
					var $img,$thisUrl,$marginToBe;
					$img=$imagesToInsert[0];
					$thisUrl=$img.url;
					if($lastArray[$thisUrl]){
						$urlArray[$thisUrl].css('marginRight',100);
					}
					switch($img.setClass){
						case'c':$urlArray[$thisUrl].attr('class','current-image');
						break;
						case'n':$urlArray[$thisUrl].attr('class','next-image');
						break;
						case'p':$urlArray[$thisUrl].attr('class','prev-image');
						break;
					}
					if($img.prepImg){
						$content.prepend($urlArray[$thisUrl]);
						if($('img:eq(0)',$content).width()!==0){
							$content.css('marginLeft',getOpWidth());
							$marginToBe=$('.next-image').position().left;
							$marginToBe+=$('.next-image').width()/2;
							$marginToBe=($wrapper.width()/2)-$marginToBe;
							if($('img:eq(0)',$content).attr('src')===$('.prev-image').attr('src')){
								$content.css('marginLeft',$marginToBe);
							}
						}
					}else{
						$content.append($urlArray[$thisUrl]);
					}
					fadeInImage($urlArray[$thisUrl]);$imagesToInsert.shift();
				}
				exNext();
			}
			function pushingImages(u,pi,c){
				var $imgPush;
				if(u){
					$imgPush=[];
					$imgPush.url=u;
					$imgPush.prepImg=pi;
					$imgPush.setClass=c;
					$imagesToInsert.push($imgPush);
					if($nextAction.indexOf(insertImages)==-1){
						$nextAction.unshift(insertImages);
					}
					if(checkImage(u)&&!$loading){
						exNext();
					}
				}
			}
function checkToCut(){
	var $lastOffset,$imgPush,$setIndex;
	$lastOffset=$('img:last',$content).offset().left;
	$imgPush=[];
	if(getOpWidth()<$('img:eq(0)',$content).width()*-1){
		$('img:eq(0)',$content).remove();$content.css('marginLeft',getOpWidth());
	}else if($('img:eq(0)',$content).offset().left>$wrapper.offset().left){
		$setIndex=$thumbArray.indexOf($('img:eq(0)',$content).attr('src'));
		pushingImages($thumbArray[$setIndex-1],true,null);
	}
	if($lastOffset-100>$wrapper.width()){
		$('img:last',$content).remove();
	}else if($lastOffset+$('img:last',$content).width()<$wrapper.width()){
		$setIndex=$thumbArray.indexOf($('img:last',$content).attr('src'));pushingImages($thumbArray[$setIndex+1],false,null);
	}
}
function hideSlider(){
	$('#header, #footer').removeClass('no-pointer');
	$('#logobox').fadeIn('fast');$wrapper.fadeOut('fast',function(){$content.empty();$imagesToInsert.length=0;$imagesToLoad.length=0;$nextAction.length=0;$this.fadeIn('fast');});}
function startUpSlider(){
	$('#header, #footer').addClass('no-pointer');
	$this.fadeOut('fast',function(){$('#logobox').fadeOut('fast');$wrapper.fadeTo('fast',0.01,function(){$content.css('marginLeft',getOpWidth());$wrapper.fadeTo('fast',1,function(){checkToCut();});});});}
function setUrlArray(){
	$thumbArray=[];
	$lastArray=[];
	$('a',$this).each(function(){
		if($(this).hasClass('inactive')){$(this).removeClass('inactive');}
		if( $(this).hasClass($curClass) ){
			$('img:eq(0)',$(this)).removeAttr('title');
			$urlArray[$(this).attr('href')]=null;
			if($(this).hasClass('add-space-right')){
				$lastArray[$(this).attr('href')]=true;
			}
			$thumbArray.push($(this).attr('href'));
		} else {
			$(this).addClass('inactive');
		}
	});
}
function thumbClick(curl,purl,nurl){pushingImages(curl,false,'c');if(purl){pushingImages(purl,true,'p');}if(nurl){pushingImages(nurl,false,'n');}$nextAction.push(startUpSlider);if(!$loading){exNext();}}
function animateThis(){
	$content.stop().animate({'marginLeft':getOpWidth()},500,function(){
		
		
		if($('img.current-image').attr('src')){
			$nextAction.push(checkToCut);exNext();
			}
		});
			
}
function galleryEdge(d){var $clickSrc,$imgExists,$curIndex,$totalIndex,$curImg;$imgExists=false;$curIndex=$('img.current-image').index();$curImg=$('img:eq('+($curIndex+d)+')',$content);$clickSrc=$curImg.attr('src');$totalIndex=$thumbArray.indexOf($clickSrc);$content.children('img').removeAttr('class');$curImg.addClass('current-image');$curIndex=$('img.current-image').index();$curImg=$('img:eq('+($curIndex+d)+')',$content);if(d===+1){$('img:eq('+($curIndex-1)+')',$content).attr('class','prev-image');if($curImg.attr('src')){$curImg.attr('class','next-image');animateThis();}else if($thumbArray[$totalIndex+d]!==undefined){$nextAction.push(insertImages);$nextAction.push(animateThis);pushingImages($thumbArray[$totalIndex+d],false,'n');}}else if(d===-1){$('img:eq('+($curIndex+1)+')',$content).attr('class','next-image');if($curImg.attr('src')){$curImg.attr('class','prev-image');animateThis();}else if(!$curImg.attr('src')&&$thumbArray[$totalIndex+d]!==undefined){$nextAction.push(insertImages);$nextAction.push(animateThis);pushingImages($thumbArray[$totalIndex+d],true,'p');}}
if($thumbArray[$totalIndex+d]===undefined){animateThis();}}
$(window).resize(function(){animateThis();});
function setIVUI(){
	$('.next-image').live({
		click:function(){galleryEdge(+1);}});
	$('.prev-image').live({
		click:function(){galleryEdge(-1);}});
	$('.current-image').live({
		click:function(){hideSlider();}});
	
	$('a.thumbnail-image').live({
		click:function(){
			var clickThumbIndex = $thumbArray.indexOf($(this).attr('href'));
			thumbClick($(this).attr('href'),$thumbArray[clickThumbIndex-1],$thumbArray[clickThumbIndex+1]);
			return false;
		}
	});
	$('.menu-box ul li a, #sub-logo a').click(function(){
		
		$curClass = $(this).attr('href');
		if($('#header').hasClass('no-pointer')) {
			setUrlArray();
			hideSlider();
		} else {
			$this.fadeOut('fast', function(){
				setUrlArray();
				$this.fadeIn('fast');
			});
		}
		return false;
	});
		
}
setUrlArray();setIVUI();});return this;};})(jQuery);