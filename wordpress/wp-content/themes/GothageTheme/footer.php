<?php $nip_class = ' class="menu-li"'; ?>
</div><!-- content end-->
</div> <!-- wrapper end. -->
<div id="footer">
<ul id="main-nav">
	<li id="index-nav"<?php if(!$GLOBALS['isIOS']){echo $nip_class;} ?>>
		<a id="index-link" href="#">Mikael Göthage Index</a>
		<div class="menu-box"><div>
			<?php 
			$taxonomy     = 'pwtist_tag';
			$orderby      = 'name';
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = 1;      // 1 for yes, 0 for no
			$title        = '';
			$empty        = 0;
			
			$args = array(
			  'taxonomy'     => $taxonomy,
			  'orderby'      => $orderby,
			  'show_count'   => $show_count,
			  'pad_counts'   => $pad_counts,
			  'hierarchical' => $hierarchical,
			  'title_li'     => $title,
			  'hide_empty'   => $empty
			);
			?>
			<ul> <strong>Sort by:</strong>
				<?php
if ($GLOBALS['isIOS']) : ?>
<?php wp_list_categories( $args ); ?>
<li><a href="<?php echo home_url( '/' ); ?>overview">Overview</a></li>
<?php else: ?>

<?php $stags = get_categories($args); ?>
<?php foreach ($stags as $stag): ?>
	<li><a href="<?php echo $stag->slug; ?>"><?php echo $stag->cat_name; ?></a></li>
<?php endforeach ?>
<li><a href="overview">Overview</a></li>
<?php endif; ?>					
			</ul>
		</div></div>
	</li>
	<li id="contact-nav"<?php if(!$GLOBALS['isIOS']){echo $nip_class;} ?>>
		<a id="contact-link" href="#">Mikael Göthage Contact</a>
		<div class="menu-box"><div>
			<?php 
			$page_id = 35;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
		</div></div>
	</li>
	<li id="about-nav"<?php if(!$GLOBALS['isIOS']){echo $nip_class;} ?>>
		<a id="about-link" href="#">Mikael Göthage About</a>
		<div class="menu-box"><div>
			<?php 
			$page_id = 2;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
		</div></div>
	</li>		
</ul>
</div><!-- footer end -->
<script>
	$(document).ready(function(){
		
		$('#thumb-chart').thumbScript();
		$('#thumb-chart').autoScroll( {upBox: '#header', downBox: '#footer', speedMod : 0.4} );
		$('#about-nav .menu-box div p').slowscroll( {classToHover: '#about-nav', speed : 1000, delay: 8000} );
		
	});
</script>
<?php if($GLOBALS['clicklogo']): ?>

<script>
	$(document).ready(function(){
		var clicked = false;
		$('#thumb-chart').css('display', 'none');
		$('#thumb-chart').css('visibility', 'hidden');
		$('#header').css('display', 'none');
		$('#footer').css('display', 'none');
		$('#logobox img').fadeIn('fast');
		$('#logobox').click(function(){
			clicked = true;
			$('#logobox').stop().fadeOut('slow');
			$('#logobox img').stop().fadeOut('slow',function(){
				$('#logobox').stop().fadeIn('slow');
				$('#logobox').addClass('no-events');
				$('#thumb-chart').css('visibility', 'visible');
				$('#thumb-chart').fadeIn('slow', function(){
					$('#header').fadeIn('slow', function(){
						$('#footer').fadeIn('slow');
					})
				});
			});
		});
		$('body').animate({'marginTop':0}, 3000, function(){
			if(clicked) {return false;}
			$('#logobox').trigger('click');
		});
	});
</script>
<?php endif; ?>
<img id="loading" src="<?php bloginfo('template_directory'); ?>/images/loading.gif" alt="loading" />
<?php wp_footer(); ?>
</body>
</html>