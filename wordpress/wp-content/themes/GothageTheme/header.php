<?php // Check if user is on ios.
	$GLOBALS['isIOS'] = false;
	$GLOBALS['isPhonePod'] = false;
	$padClass = '';
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') ||
		strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
		strpos($_SERVER['HTTP_USER_AGENT'], 'iPod')) {
		$GLOBALS['isIOS'] = true;
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
			$padClass = ' class="ipad"';
		}
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
			strpos($_SERVER['HTTP_USER_AGENT'], 'iPod')) {
				$GLOBALS['isPhonePod'] = true;
			}
	}
?>
<?php if(is_front_page()){$GLOBALS['clicklogo'] = true;} ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php echo bloginfo('name'); ?> - <?php echo bloginfo('description'); ?></title
	<meta name="Subject" content="Photographer, Fotograf, Photography, Fotografi">
	<meta name="Keywords" content="fotograf, photographer, reklamfotografi, advertising photography, livsstilsfotografi, lifestyle photography, modefotografi, fashion photography, stilleben, still life, digital fotografi, digital photography, kreativ, creative, retusch, retouching, göteborg, gothenburg, sverige, sweden, livsstil, lifestyle, reklam, advertising, digital, analog, analouge, studio, porträtt, portrait, reklamfotograf, advertising photograhper, fotografi, photography, digitalfoto, digital photo, bilder, images, imagery, photographs, musikfotografi, music photography, musikfoto, music photo, musik, music">
	<meta name="Description" content="Fotograf Mikael Göthage, Göteborg. Photographer Mikael Gothage, Gothenburg.">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	
	<?php if($GLOBALS['isIOS']): //iOs specific ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/mobile2.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/styles.css" />
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/iscroll-lite.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/klass.min.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/code.photoswipe.jquery-2.1.6.min.js"></script> 
		<script type="text/javascript">
		$(document).ready(function(){
			window.scrollTo(0, 1);
			var myScroll;
			function loaded() {
				setTimeout(function () {
					myScroll = new iScroll('content',{zoom:true});
				}, 100);
			}
			window.addEventListener('load', loaded, false);
			var myPhotoSwipe = $("#thumb-chart a").photoSwipe({allowUserZoom:false,enableMouseWheel: false , enableKeyboard: false });
			$('*').click(function(e){
				if (e.target !== this) {return;}
				var thisid = $(this).parent('li').attr('id');
				if( thisid === 'index-nav' || thisid === 'contact-nav' || thisid === 'about-nav' ){
					if( $('.active').parent('li').attr('id') != thisid ) {$('.active').removeClass('active');}
					if($(this).siblings('.menu-box').hasClass('active')){$(this).siblings('.menu-box').removeClass('active');
					} else {$(this).siblings('.menu-box').addClass('active');}
				} else { $('#index-nav a, #contact-nav a, #about-nav a').siblings('.menu-box').removeClass('active');}
			});
		});
		</script>
	<?php else : // all else ?>
		
		<script type="text/javascript">
		
		
		var image1 = $('<img />').attr('src', 'http://gothage.com/wordpress/wp-content/themes/GothageTheme/images/logoblack.png');
		</script>
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/scroll.css" />
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery-ishovered.js"></script> 
		<!--[if !IE]><!-->
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/thumbScript.js"></script> 
		<!--<![endif]-->
		<!--[if IE]>
		  <link href="<?php bloginfo('template_url') ?>/css/iecss.css" rel="stylesheet" type="text/css">
		  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/thumbScriptIE.js"></script> 
		<![endif]-->
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/autoScroll.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/slowscroll.js"></script> 
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body>
	<div id="wrapper"<?php echo $padClass; ?>>
		<div id="header">
			<h1 id="sub-logo" class="gothage-logo"><a href="overview">Mikael Göthage</a></h1>
		</div><!-- header end. -->
				<div id="content">