<?php get_header(); ?>
<h3 style="display:none">Mikael Götehage Photographs Tag: <?php single_tag_title(); ?></h3>
<div id="thumb-chart">
	

<?php while(have_posts()): the_post(); ?>

<?php if(has_post_thumbnail()): 
$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
?>
<a class="thumbnail-image" href="<?php echo $full_image_url[0]; ?>">
<?php the_post_thumbnail('thumbnail'); ?>
</a>
<?php endif; ?>


<?php endwhile; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>