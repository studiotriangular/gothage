<?php /* Template Name: Mobil */?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); $site_description = get_bloginfo( 'description', 'display' ); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; ?></title>
		<meta name = "viewport" content = "width = device-width">
		<meta name = "viewport" content = "initial-scale = 1.0">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/mobile.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/iscroll-lite.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.masonry.min.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/code.photoswipe.jquery-2.1.6.min.js"></script> 
		<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			
			window.scrollTo(0, 1);
			//setTimeout(function () {  }, 0);
			
			var updateLayout = function() {
				//myScroll.refresh();
				if (window.innerWidth != currentWidth) {
					currentWidth = window.innerWidth;
					var orient = (currentWidth == 320) ? "profile" : "landscape";
					document.body.setAttribute("orient", orient);
					window.scrollTo(0, 1);
				} 
				iPhone.DomLoad(updateLayout);
				setInterval(updateLayout, 500);
				};
			});
			
			var myScrolls;
			function loaded() {
				setTimeout(function () {
					$('.thumbnails').each(function(){
						$idtouse = $(this).attr('id');
						scroll = new iScroll($idtouse, {snap : 'a'});
					});
					//myScroll = new iScroll('wrapper', snap : false);
				}, 100);
			}
			window.addEventListener('load', loaded, false);
    	</script>
	</head>
	<body>
		<header>
			<h1 id="sub-logo"><a href="#overview-thumbs">Mikael Göthage</a></h1>
		</header>
		
		
<?php 
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order'));

$pwtist_args = array('taxonomy' => 'pwtist_tag');
$pwtist_tags = get_categories($pwtist_args);
$pts = array();
$pts['overview'] = '<div id="overview-thumbs" class="thumbnails"><div>';

$galleries = array();
$galleries['overview'] = '<div id="overview-gallery" class="gallery">';


foreach($pwtist_tags as $the_tag) {
	$pts[$the_tag->slug] = '<div id="'.$the_tag->slug.'-thumbs" class="thumbnails"><div>'.PHP_EOL;
	$galleries[$the_tag->slug] = '<div="'.$the_tag->slug.'-gallery" class="gallery">'.PHP_EOL;
}




?>
<!-- index -->
<div id="sort-menu" class="menu-page">
	<nav>
		<ul>
			<li><a href="#overview-thumbs">Overview</a></li>
			<?php
			foreach($pwtist_tags as $the_tag)  :?>
				<li><a href="#<?php echo $the_tag->slug ?>-thumbs"><?php echo $the_tag->name; ?></a></li>
			<?php
			endforeach;
?>
		</ul>
	</nav>
</div>


<div class="menu-page" id="contact">
			<?php 
			$page_id = 35;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
</div>


<div class="menu-page" id="about">
			<?php 
			$page_id = 2;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
</div>


<?php /* BEGIN HANDLING LOOPS */ ?>

<?php foreach($cat_sorts as $key) : ?>
	<?php foreach($key as $slug => $set) : ?>
		<?php 
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $slug); 
		query_posts($new_args);
		
		
		
		if(have_posts()):?>
			<?php
			// add on cat holder to each thumb section
			
			
			
			?>
			<?php while(have_posts()):the_post(); ?>
				<?php 
				$t_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail');
				$f_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
				$pwtist_info 	= get_post_meta($post->ID, 'pwtist_image_cap', true);
				
				$terms = get_the_terms( $post->ID, 'pwtist_tag' ); 
				
				$a_link = '';
				$a_style = 'style="width:'.$t_url[1].'px; height:'.$t_url[2].'px; background-image:url('.$t_url[0].');"';
				$a_complete = '<a href="#" '.$a_style.' class="thumbnail">&nbsp;</a>';
				$pts['overview'] .= $a_complete.PHP_EOL;
				foreach($terms as $term) {
					$a_link = '';
					$pts[$term->slug] .= $a_complete.PHP_EOL;
					
				}
				
				
				
				?>
			<?php endwhile; // end while have posts ?>
		<?php endif; // end if have posts ?>
	<?php endforeach; // end $key as $slug => $set ?>
<?php endforeach; // end $cat_sorts as $key ?>
<?php
		foreach($pts as $pts) {
			echo $pts."</ul>".PHP_EOL;
		}
?>


		
		<footer>
			<nav>
				<ul>
					<li id="index-nav"><a href="#sort-menu">Index</a></li>
					<li id="contact-nav"><a href="#contact">Contact</a></li>
					<li id="about-nav"><a href="#about">About</a></li>
				</ul>
			</nav>
		</footer>
		<script>myScroll = new iScroll('scroller', { checkDOMChanges: false, momentum:true, hScrollbar:false, hScroll:false});</script>
	</body>
</html>