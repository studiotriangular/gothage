<?php /* Template Name: Mobilen
Före jag testar att implementera mobil variant i befintlig sida.
 */?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); $site_description = get_bloginfo( 'description', 'display' ); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; ?></title>
		<meta name = "viewport" content = "width = device-width">
		<meta name = "viewport" content = "initial-scale = 1.0">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/mobile.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/styles.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/iscroll-lite.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.masonry.min.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/klass.min.js"></script> 
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/code.photoswipe.jquery-2.1.6.min.js"></script> 
		<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			
			window.scrollTo(0, 1);
			/*IPHONE SETUP */
			var updateLayout = function() {
				if (window.innerWidth != currentWidth) {
					currentWidth = window.innerWidth;
					var orient = (currentWidth == 320) ? "profile" : "landscape";
					document.body.setAttribute("orient", orient);
					window.scrollTo(0, 1);
				} 
				iPhone.DomLoad(updateLayout);
				setInterval(updateLayout, 500);
				};
			});
			
			
			/* SETUP ISCROLL*/
			var myScrolls;
			function loaded() {
				setTimeout(function () {
					$('.thumbnails').each(function(){
						$idtouse = $(this).attr('id');
						
						scroll = new iScroll($idtouse, {snap : 'a'});
					});
				}, 100);
			}
			window.addEventListener('load', loaded, false);
			
			
			
			/* setup swipe */
			(function(window, $, PhotoSwipe){
				
				$(document).ready(function(){
					
					var options = {zIndex : 50, allowUserZoom : false};
					$('.thumbnails').each(function(){
						$idtouse = $(this).children('.image-list').attr('id');
						$('#'+$idtouse+' a').photoSwipe(options);
					});
					
					//$("#overview-thumbs a").photoSwipe(options);
				
				});
				
				
			}(window, window.jQuery, window.Code.PhotoSwipe));
    	</script>
	</head>
	<body>
		<header>
			<h1 id="sub-logo"><a href="#overview-thumbs">Mikael Göthage</a></h1>
		</header>
		
		
<?php 
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order'));

$pwtist_args = array('taxonomy' => 'pwtist_tag');
$pwtist_tags = get_categories($pwtist_args);
$pts = array();
$pts['overview'] = '<div id="overview-thumbs" class="thumbnails"><ul id="overview-thumbs-list" class="image-list">'.PHP_EOL;




foreach($pwtist_tags as $the_tag) {
	$pts[$the_tag->slug] = '<div id="'.$the_tag->slug.'-thumbs" class="thumbnails"><ul id="'.$the_tag->slug.'-thumbs-list" class="image-list">'.PHP_EOL;
}




?>
<!-- index -->
<div id="sort-menu" class="menu-page">
	<nav>
		<ul>
			<strong>Sort by:</strong>
			<li><a href="#overview-thumbs">Overview</a></li>
			<?php
			foreach($pwtist_tags as $the_tag)  :?>
				<li><a href="#<?php echo $the_tag->slug ?>-thumbs"><?php echo $the_tag->name; ?></a></li>
			<?php
			endforeach;
?>
		</ul>
	</nav>
</div>


<div class="menu-page" id="contact">
			<?php 
			$page_id = 35;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
</div>


<div class="menu-page" id="about">
			<?php 
			$page_id = 2;
			$page_data = get_page($page_id);
			$content = apply_filters('the_content', $page_data->post_content);
			echo $content;
		 	?>
</div>


<?php /* BEGIN HANDLING LOOPS */ ?>

<?php foreach($cat_sorts as $key) : ?>
	<?php foreach($key as $slug => $set) : ?>
		<?php 
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $slug); 
		query_posts($new_args);
		
		
		
		if(have_posts()):?>
			<?php
			// add on cat holder to each thumb section
			
			
			
			?>
			<?php while(have_posts()):the_post(); ?>
				<?php 
				$t_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail');
				$f_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
				$pwtist_info 	= get_post_meta($post->ID, 'pwtist_image_cap', true);
				
				$terms = get_the_terms( $post->ID, 'pwtist_tag' ); 
				$img_src = $t_url[0];
				$a_href = $f_url[0];
				
				$img_str = '<li><a href="'.$a_href.'"><img src="'.$img_src.'" alt="'.$pwtist_info.'"/></a></li>';
				$pts['overview'] .= $img_str.PHP_EOL;
				foreach($terms as $term) {
					$pts[$term->slug] .= $img_str.PHP_EOL;
					
				}
				
				
				
				?>
			<?php endwhile; // end while have posts ?>
		<?php endif; // end if have posts ?>
	<?php endforeach; // end $key as $slug => $set ?>
<?php endforeach; // end $cat_sorts as $key ?>
<?php
		foreach($pts as $pts) {
			echo $pts."</ul></div>".PHP_EOL;
		}
?>


		
		<footer>
			<nav>
				<ul>
					<li id="index-nav"><a href="#sort-menu">Index</a></li>
					<li id="contact-nav"><a href="#contact">Contact</a></li>
					<li id="about-nav"><a href="#about">About</a></li>
				</ul>
			</nav>
		</footer>
		
	</body>
</html>