<?php /* Template name: mobile */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	
	
		
		
	
	
	
	<?php wp_head(); ?>
</head>


	<body>
		<?php get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order')); ?>
<?php /* BEGIN OVERVIEW */ 

$overview = array();

foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
	
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $value, 'order_by' => 'name', 'order'=>'ASC'); 
		query_posts($new_args);?>	
		<?php if(have_posts()): ?> 
			
				<?php while(have_posts()) : the_post();?>
			
					<?php if(has_post_thumbnail()) :
							if ($GLOBALS['isPhonePod']) {
								$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
							} else {
								$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
							}
							?>
							<?php $overview['images'][] = array('src' => $full_image_url[0]); ?>
					<?php endif; ?>
				<?php endwhile; ?>
			
		<?php endif;wp_reset_query(); ?>
	<?php endforeach; ?>
<?php endforeach; ?>

<?php echo json_encode($overview); ?>
	</body>
</html>