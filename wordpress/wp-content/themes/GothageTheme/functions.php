<?php 
function getTinyUrl($url) {
    $tinyurl = file_get_contents("http://tinyurl.com/api-create.php?url=".$url);
    return $tinyurl;
}
add_theme_support('post-thumbnails');

add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );

?>