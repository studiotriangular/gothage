<?php
/*Template Name: frontpage */ 
?>

<?php get_header(); ?>
<a id="intrologo" class="hidden" href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logoblack.png" alt="Mikael Göthage Photographs"/></a>
<?php get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order')); ?>
<div id="thumb-chart" class="hidden">
<?php get_template_part('parts/category', 'loop' ); ?>
</div><!-- thumb-chart end -->


<div id="iv-wrapper" class="closed"><div id="iv-content"></div></div>


<?php get_footer(); ?>






