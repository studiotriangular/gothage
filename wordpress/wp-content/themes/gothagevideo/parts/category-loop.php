<?php /* BEGIN HANDLING LOOPS */ 
get_option('category_order') == "" ? "" : $cat_sorts = unserialize(get_option('category_order'));
foreach ($cat_sorts as $key) :
	foreach ($key as $value => $set) :
		$new_args = array('post_type' => 'pwtist_post', 'pwtist_cat' => $value, 'order_by' => 'name', 'order'=>'ASC'); 
		$catQuery = new WP_Query($new_args);
		if($catQuery->have_posts()): ?> 
			<div class="cat-holder">
				<?php while($catQuery->have_posts()) : $catQuery->the_post();?>
					<?php if(has_post_thumbnail()) :
							$terms = get_the_terms( $post->ID, 'pwtist_tag' ); 
							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');?>
							
							<?php // BUILD IMAGE LINK

							$a_classes = 'thumbnail-image overview';
							if( get_post_meta($post->ID, 'mp4', true) ) {
								$a_classes.= ' videopost';
							}
							// Get video meta 
							$ogg = get_post_meta( $post->ID, 'ogg', true );
							$mp4 = get_post_meta( $post->ID, 'mp4', true );
							$webm = get_post_meta( $post->ID, 'webm', true );

							$ogghires = get_post_meta( $post->ID, 'ogg-hires', true );
							$mp4hires = get_post_meta( $post->ID, 'mp4-hires', true );
							$webmhires = get_post_meta( $post->ID, 'webm-hires', true );

							foreach($terms as $term) {$a_classes .= ' ' . $term->slug;}
							if($catQuery->current_post == $catQuery->post_count-1) {$a_classes .= ' add-space-right';}
							$videodatastring = 'data-ogg="'.$ogg.'" data-mp4="'.$mp4.'" data-webm="'.$webm.'"'.' data-ogghires="'.$ogghires.'" data-mp4hires="'.$mp4hires.'" data-webmhires="'.$webmhries.'"';
							$a_link = '<a '.$videodatastring.' href="'.$full_image_url[0].'" class="'.$a_classes.'">';
							echo $a_link;
							the_post_thumbnail('thumbnail');
							?>
							</a>	
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		<?php endif; wp_reset_query(); ?>
	<?php endforeach; ?>
<?php endforeach; ?>