<?php 


function go_maintenance_mode() {
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
        wp_die('Sorry, but we are doing Maintenance on the site, please check back soon.');
    }
}
// add_action('get_header', 'go_maintenance_mode');

add_action('wp_enqueue_scripts', 'mmm_scripts_and_styles');
add_action( 'init', 'initfuncs' );
add_action('widgets_init', 'registersidebarsh2h');
add_theme_support('post-thumbnails');




function initfuncs() {
	
	register_nav_menus(
		array(
			'main_nav' => 'Main Nav',
		)
	);
}
function mmm_scripts_and_styles() {
  
	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  
		// Register top scripts
	wp_deregister_script('jquery');
	wp_register_script( 'jquery', get_stylesheet_directory_uri().'/f5/bower_components/foundation/js/vendor/jquery.js', '', '', false );
	wp_register_script( 'modernizr', get_stylesheet_directory_uri().'/f5/bower_components/foundation/js/vendor/modernizr.js', '', '', false );

	// Register bottom scripts
	wp_register_script( 'foundation', get_stylesheet_directory_uri() . '/f5/bower_components/foundation/js/foundation.min.js', array( 'jquery' ), '', true );
	wp_register_script( 'app', get_stylesheet_directory_uri().'/f5/js/app.js', array('foundation'), '', true );
	    
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'modernizr' );
	wp_enqueue_script( 'foundation' );
	wp_enqueue_script( 'app' );

	// styles
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'sassy', get_stylesheet_directory_uri().'/f5/stylesheets/app.css' );
  
}


function registersidebarsh2h(){
	 register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
}


