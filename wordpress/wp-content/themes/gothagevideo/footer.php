
</div><!-- content end-->
<footer class="hidden">
	<nav>
		<ul>
			<li id="index-nav" ><a href="#gothage-index">Mikael Göthage Index Nav</a></li>
			<li id="contact-nav" ><a href="#gothage-contact">Contact Mikael Göthage</a></li>
			<li id="about-nav"><a href="#gothage-about">About Mikael Göthage</a></li>
		</ul>
	</nav>
	<article id="gothage-index">
		<div>
		<ul>
			<li class="sort">Sort by:</li>
		<?php 
		$taxonomy     = 'pwtist_tag';
		$orderby      = 'name';
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no
		$title        = '';
		$empty        = 0;
		
		$args = array(
		  'taxonomy'     => $taxonomy,
		  'orderby'      => $orderby,
		  'show_count'   => $show_count,
		  'pad_counts'   => $pad_counts,
		  'hierarchical' => $hierarchical,
		  'title_li'     => $title,
		  'hide_empty'   => $empty
		);
		$stags = get_categories($args); ?>
		<?php foreach ($stags as $stag): ?>
			<li><a href="<?php echo $stag->slug; ?>"><?php echo $stag->cat_name; ?></a></li>
		<?php endforeach ?>
		<li><a href="overview">Overview</a></li>
		</ul>
		</div>
	</article>
	<article id="gothage-contact">
		<div>
		<?php 
		$page_id = 35;
		$page_data = get_page($page_id);
		$content = apply_filters('the_content', $page_data->post_content);
		echo $content;
	 	?>
	 	</div>
	</article>
	<article id="gothage-about">
		<div>
		<?php 
		$page_id = 2;
		$page_data = get_page($page_id);
		$content = apply_filters('the_content', $page_data->post_content);
		echo $content;
	 	?>
	 	</div>
	</article>

</footer>
<img id="loading" src="<?php bloginfo('template_directory'); ?>/images/loading.gif" alt="loading" />
  <?php wp_footer(); ?>
</body>
</html>